import pandas as pd


def print_statistics(df):
    """
    Print descriptive statistics to console for the input dataframe.

    :param pd.DataFrame df: the dataframe to print statistics for.
    """
    print("\n--------------- HEAD ----------------------")
    print(df.head())
    print("\n--------------- DESCRIPTIVE ---------------")
    print(df.describe())
    print("\n--------------- SUMMARY -------------------")
    print(df.info())
    print("\n--------------- VALUE COUNTS --------------")
    for column in df.columns:
        print("\n", df[column].value_counts())


def read_machine_data(print_stats=False):
    """
    Reads and returns dataframe from file for machine inspection (temperature vs. pressure).

    :param bool print_stats: if True prints descriptive statistics to console.
    :return pd.DataFrame: dataframe with data loaded from machine_inspection file.
    """
    df = pd.read_csv(
        filepath_or_buffer="../data/machine_inspection.txt",
        header=None,
        delimiter=r"\s+",
        names=["temperature", "pressure"]
    )
    if print_stats:
        print_statistics(df)
    return df


def read_credit_data(print_stats=False):
    """
    Reads and returns dataframe from file for fraudulent credit card transactions.

    :param bool print_stats: if True prints descriptive statistics to console.
    :return pd.DataFrame: dataframe with data loaded from machine_inspection file.
    """
    df = pd.read_csv("../data/creditcard.csv")
    if print_stats:
        print_statistics(df)
    return df
