import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.mixture import GaussianMixture
from sklearn.metrics import fbeta_score, classification_report
from wranglers import read_credit_data


def plot_distributions():
    """Plot the distribution of columns in the credit fraud dataset (save to file)."""

    # Read credit fraud dataset.
    df = read_credit_data(print_stats=False)

    # Define batches of columns to plot distributions for.
    num_v_cols_to_plot = 5
    columns = ["Time", "Amount"] + ["V" + str(index + 1) for index in range(num_v_cols_to_plot)]
    break_indices = (2, 2 + num_v_cols_to_plot)

    # Iterate over column batches and plot each batch to file.
    last_break = 0
    for this_break in break_indices:

        # Select batch of columns according to breakpoint.
        these_columns = columns[last_break:this_break]

        # Create figure with an axis for each selected column.
        num_axes = len(these_columns)
        fig, ax = plt.subplots(nrows=1, ncols=num_axes, figsize=(num_axes * 3, 3))

        # Fill axes with selected column distributions.
        for this_ax, this_column in zip(ax, these_columns):
            sns.kdeplot(data=df, x=this_column, hue="Class", common_norm=False, fill=True, ax=this_ax)

        plt.tight_layout()
        plt.show(block=False)
        plt.savefig(f"../images/credit_fraud_dists_cols_{last_break}_to_{this_break - 1}.png")

        # Remember last break point.
        last_break = this_break


def load_date():
    """
    Loads credit card data from file and loads it into numpy arrays.

    :return tuple[np.ndarray, np.ndarray]: X, y feature and label sets.
    """

    # Read credit card fraud dataset.
    df = read_credit_data(print_stats=False)

    # Define feature and target columns.
    targets = ["Class"]
    features = [column for column in df.columns if column not in targets]

    # Load into numpy arrays.
    return df[features].values, df[targets].values


def do_a_better_train_test_split(X, y):
    """
    Creates a randomized train-test split such that all anomalies are in the test set (none are in the train set) and
    proportion of anomalies in test set is 1%.

    :param np.ndarray X: feature set of all observations.
    :param np.ndarray y: label for all observations
    :return tuple[np.ndarray, np.ndarray,np.ndarray, np.ndarray]: train-test split X_train, X_test, y_train, y_test.
    """

    # Extract the number of anomalies observed in the entire dataset.
    num_anomalies = y[y == 1].shape[0]

    # Create boolean masks for anomalies and normal observations.
    anomaly_mask = y.reshape(-1, ) == 1
    normal_mask = ~anomaly_mask

    # Split normal observations into train and test such there is 99:1 ratio of test set size and number of anomalies.
    X_train, X_test, y_train, y_test = train_test_split(
        X[normal_mask],
        y[normal_mask],
        test_size=num_anomalies * 99,
        random_state=42
    )

    # Add all anomalies to train set.
    X_test = np.concatenate([X_test, X[anomaly_mask]], axis=0)
    y_test = np.concatenate([y_test, y[anomaly_mask]], axis=0)

    # Assert that there is 1% anomalies in the test set and none in the train set.
    assert y_test[y_test == 1].shape[0] / y_test.shape[0] == 0.01
    assert y_train[y_train == 1].shape[0] / y_train.shape[0] == 0

    # Return train test split.
    return X_train, X_test, y_train, y_test


def train_and_score_many_gmms(X_train, X_test, y_train, y_test, verbose=True):
    """
    Train multiple GMM models on the train set and score each one on the test set for multiple values of threshold
    for the wighted log probabilities to be considered an anomaly. Uses F2-score as a metric (to weight recall more
    than precision). Returns a dictionary with all the hyperparameters attempted and corresponding results,
    and a dictionary with the best hyperparameter combination and the best fitted model.

    :param np.ndarray X_train: features for train set.
    :param np.ndarray X_test: features for test set.
    :param np.ndarray y_train: labels for train set.
    :param np.ndarray y_test: labels for test set.
    :param bool verbose: if True prints updates to console.
    :return tuple[dict, dict]: hyperparameter tuning results dictionary, and best model and result dictionary
    """

    # Scale the features according to train set.
    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    # Set hyperparameter tuning ranges.
    components = [3, 4, 5, 6, 7, 8]  # k ~ sqrt(number_of_features).
    thresholds = -np.arange(0, 1001)  # Because why not?

    # Iterate over number of mixture components, train a model at each iteration, and score for optimal threshold.
    results = {"score": [], "threshold": [], "n_components": []}
    best = {"score": 0, "threshold": None, "n_components": None, "model": None}
    for this_number_of_components in components:

        # Train a Gaussian mixture models on train set with this_number_of_components.
        this_model = GaussianMixture(n_components=this_number_of_components, random_state=42)
        this_model.fit(X_train)

        # Calculate the weighted log probabilities for the test set (recall: 1% anomalies).
        weighted_log_probs = this_model.score_samples(X_test)

        # Score the model on the test set with each threshold in the range (with fine granularity).
        scores = []
        for threshold in thresholds:
            scores.append(fbeta_score(y_true=y_test, y_pred=weighted_log_probs < threshold, beta=2))
        scores = np.array(scores)

        # Extract optimal threshold for best score with this_number_of_components.
        this_best_threshold = thresholds[np.argmax(scores)]
        this_best_score = np.max(scores)

        # Append optimal result to results list.
        results["score"].append((this_best_score, scores))
        results["threshold"].append((this_best_threshold, thresholds))
        results["n_components"].append(this_number_of_components)

        # Check if this is the best one thus far (prefer model with less components, less likely to over-fit) .
        if this_best_score > best["score"]:
            best["score"] = this_best_score
            best["threshold"] = this_best_threshold
            best["n_components"] = this_number_of_components
            best["model"] = this_model
            best["y_preds"] = weighted_log_probs < this_best_threshold

        # Report result to console.
        if verbose:
            print(
                f"Using {this_number_of_components} components "
                f"the optimal threshold is {this_best_threshold} "
                f"for an F2-score of {this_best_score}"
            )

    # Print to console the best model.
    if verbose:
        print("--------------------------- BEST MODEL SCORE --------------------------------")
        print(f"Gaussian Mixture Model with {best['n_components']} components and a threshold of {best['threshold']}.")
        print(f"The F2-score of the model is: {best['score']}")
        print("--------------------------- CLASSIFICATION REPORT ---------------------------")
        print(classification_report(y_true=y_test, y_pred=best['y_preds']))
        print("-----------------------------------------------------------------------------")

    # Return all the results bundled together in dictionaries.
    return best, results


def plot_tuning_results(best, results):
    """
    Plots the hyperparameter tuning results provided by the function train_and_score_many_gmms.

    :param dict best: best result and hyper parameter combination.
    :param dict results: all results and hyper parameter combinations attempted.
    """

    # Create a new figure and axis.
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 7))

    # Plot score vs. threshold curve for ech number of components.
    for index, this_number_of_components in enumerate(results["n_components"]):
        best_score, scores = results["score"][index]
        best_threshold, thresholds = results["threshold"][index]
        ax.plot(thresholds, scores, label=f"n_components = {this_number_of_components}")
        ax.plot(best_threshold, best_score, 'o', ms=12, color="red")

    # Annotate figure.
    ax.set_title(f"Gaussian Mixture Model: anomaly detection (best F-score = {best['score']:.3f})")
    ax.set_xlabel("Threshold to detect anomaly given weighted log probability")
    ax.set_ylabel("F2-score")
    ax.legend()
    ax.set_ylim((0, 1))

    # Show without clipping or blocking and save image.
    plt.grid()
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("../images/credit_anomalies_gmm.png")


def main():
    """
    Load credit card fraud dataset into numpy arrays. Upsample anomaly class to 1% in test set (use all anomalies)
    and leave the rest for train set (randomized). Train GMMs for anomaly detection experimenting with different
    thresholds and number of components (print results to console). Plot the tuning results (save to file).
    """
    plot_tuning_results(
        *train_and_score_many_gmms(
            *do_a_better_train_test_split(
                *load_date()
            )
        )
    )


if __name__ == "__main__":
    main()
