import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest
from wranglers import read_machine_data


# TODO separate plot and train functions (and in dbscan).
def isolation_forest():
    """Anomaly detection from machine data using isolation forest and scaler preprocessing."""

    # Load and scale petri dish data and extract points as numpy arrays.
    df = read_machine_data(print_stats=False)
    scaler = StandardScaler()
    X = scaler.fit_transform(df.values)

    # Scale test (labeled) set with the scaler (fit to train set).
    points_normal = scaler.transform(np.array([[470, 505], [642, 634]]))
    points_abnormal = scaler.transform(np.array([[450, 650], [625, 400]]))

    # Fit an isolation forest model to the train set.
    model = IsolationForest(n_estimators=200, max_samples=256, contamination=0.03, random_state=42)
    model.fit(X)

    # Predict on train and test sets (and unify into a combined array of -1 and 1 labels).
    labels_train = model.predict(X)
    labels_test_normal = model.predict(points_normal)
    labels_test_abnormal = model.predict(points_abnormal)

    # Unify into a combined array of data points and labels (-1 and 1).
    X_all = np.concatenate([X, points_normal, points_abnormal])
    labels_all = np.concatenate([labels_train, labels_test_normal, labels_test_abnormal])

    # plot the line, the samples, and the nearest vectors to the plane
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(14, 7))
    names = {
        "x": r"temperature [$\sigma$]",
        "y": r"pressure [$\sigma$]"
    }

    # In the first subplot, plot the model decision function over a 2D grid in the background.
    sigmas = 4
    resolution = 80
    x, y = np.meshgrid(
        np.linspace(-sigmas, sigmas, resolution),
        np.linspace(-sigmas, sigmas, resolution)
    )
    decision_function = model.decision_function(np.c_[x.ravel(), y.ravel()]).reshape(x.shape)
    ax[0].contourf(x, y, decision_function, cmap=plt.cm.get_cmap("Greys"))

    # And the train and test set points in the foreground.
    s1 = ax[0].scatter(X[:, 0], X[:, 1], c="white", s=20, edgecolor="black")
    s2 = ax[0].scatter(points_normal[:, 0], points_normal[:, 1], c="green", s=30, edgecolor='k')
    s3 = ax[0].scatter(points_abnormal[:, 0], points_abnormal[:, 1], c="red", s=30, edgecolor='k')

    # In the second subplot, plot train and test set points and color them by predicted label.
    ax[1].scatter(X_all[:, 0], X_all[:, 1], c=pd.Series(labels_all).map({1: "green", -1: "red"}), s=20,
                  edgecolor="black")

    # And highlight the test set points with circles colored by their ground-truth labels.
    for (px, py), (sx, sy) in zip(points_normal, points_abnormal):
        ax[1].plot(px, py, 'o', ms=12, markerfacecolor="None", markeredgecolor='green', markeredgewidth=3)
        ax[1].plot(sx, sy, 'o', ms=12, markerfacecolor="None", markeredgecolor='red', markeredgewidth=3)

    # Annotate both subplots.
    for this_ax in ax:
        this_ax.set_xlabel(names["x"])
        this_ax.set_ylabel(names["y"])
        this_ax.axis("equal")
    ax[0].legend([s1, s2, s3], ["Train set", "Test set (normal)", "Test set (abnormal)"], loc="upper left")
    ax[0].set_title("Isolation tree decision function")
    ax[1].set_title("Isolation tree anomaly detection")

    # Show without clipping or blocking and save image.
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("../images/machine_anomalies_it.png")


if __name__ == "__main__":
    isolation_forest()
