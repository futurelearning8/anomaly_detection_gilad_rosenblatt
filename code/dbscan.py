import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from wranglers import read_machine_data


def dbscan():
    """Anomaly detection from machine data using DBSCAN and scaler preprocessing."""

    # Load and scale petri dish data and extract points as numpy arrays.
    df = read_machine_data(print_stats=False)
    scaler = StandardScaler()
    X = scaler.fit_transform(df.values)

    points_normal = scaler.transform(np.array([[470, 505], [642, 634]]))
    points_abnormal = scaler.transform(np.array([[450, 650], [625, 400]]))
    X = np.concatenate([X, points_normal, points_abnormal])

    # Fit a k-means and DBSCAN clustering models to the data.
    model1 = KMeans(n_clusters=2, random_state=42).fit(X)
    model2 = DBSCAN(eps=0.3, min_samples=10).fit(X)

    # Plot the clusters side-by-side.
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(14, 7))
    names = {
        "x": r"temperature [$\sigma$]",
        "y": r"pressure [$\sigma$]"
    }

    df = pd.DataFrame(X, columns=names.values())
    sns.scatterplot(x=names["x"], y=names["y"], data=df, hue=pd.Series(model1.labels_).map({0: "slow", 1: "fast"}), ax=ax[0])
    sns.scatterplot(x=names["x"], y=names["y"], data=df, hue=pd.Series(model2.labels_), ax=ax[1])

    # Mark test points on the image.
    for (px, py), (sx, sy) in zip(points_normal, points_abnormal):
        ax[1].plot(px, py, 'o', ms=10, markerfacecolor="None", markeredgecolor='green', markeredgewidth=3)
        ax[1].plot(sx, sy, 'o', ms=10, markerfacecolor="None", markeredgecolor='red', markeredgewidth=3)

    # Scale axes.
    ax[0].axis("equal")
    ax[1].axis("equal")

    # Annotate.
    ax[0].set_title("K-means clustering")
    ax[1].set_title("Density based scan clustering")

    # Show without clipping or blocking and save image.
    plt.tight_layout()
    plt.show(block=False)
    plt.savefig("../images/machine_anomalies_db.png")


if __name__ == "__main__":
    dbscan()
